YEARS=$(shell git ls-tree --name-only HEAD | egrep ^[0-9]{4}$)
all:
	@for y in $(YEARS) ; do $(MAKE) -C $$y/ ; done
clean:
	@for y in $(YEARS) ; do $(MAKE) -C $$y/ clean ; done
distclean:
	@for y in $(YEARS) ; do $(MAKE) -C $$y/ distclean ; done
