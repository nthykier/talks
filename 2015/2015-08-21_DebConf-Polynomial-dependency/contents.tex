\section{Introduction}

%This talk will touch on the following subjects:
%
%    What makes the problems "hard"
%    - Origin of the issues (graph -> (hypergraph, nega-deps) -> now)
%    What in turn makes the problems highly tractable in practise.
%    Various tricks to reduce the problem even further.
%    - Eqv. packages
%    - Optimise for no-backtracking
%

\begin{frame}{Introduction}
  What do I want to achieve with this talk?
  \begin{itemize}
  \item I hope to debunk some myths about what makes dependency resolution ``really hard'' and ``tractable''.
  \item I will be sharing some of our findings from optimizing Britney.  Not all will apply to you(r problem).
  \item For some, I will be stating ``the obvious'' and ``nothing new''.
  \item I do \textit{not} have the ``one-size-fits-all'' solution.  Sorry.
  \end{itemize}
\end{frame}

\begin{frame}{Defining the problem the problem}
  \begin{itemize}
  \item When you run ``apt install eclipse'', two distinct problems
    are solved before the package is installed.
    \begin{enumerate}
    \item Apt figures out what is needed to install the package.
    \item Apt/dpkg computes a series of ``unpack'', ``configure'' etc.
      actions.
    \end{enumerate}
  \item The first problem is what I will be talking about.
  \item The second problem is basically an ``install plan'' or an ``ordering problem''.
  \end{itemize}
\end{frame}

\begin{frame}{Why skip over the ``install plan''}
  \begin{itemize}
  \item Despite being an important part of installing packages, it is
    \textit{not} dependency resolution.
  \item Assuming we had no cycles\footnote{Known invalid assumption}, this
    is a trivial partial-ordering problem:
    \begin{itemize}
    \item Define all actions (e.g. ``unpack pkg/version/arch'').
    \item Compute partial-ordering constrains (``configure pkgA/...'' before ``configure pkgB/...'')
    \item Sort items such that all their constrains are satisfied.
    \item Given no cycles, we always can construct a directed-acyclic graph (DAG).
    \item Said DAG is your install plan - ``just'' feed it to dpkg and you are done.
    \end{itemize}
  \end{itemize}

  Run-time of all of this?
\end{frame}

\begin{frame}{Run-time of the ``install plan''}
  \begin{itemize}
  \item Something like graph search $O(|V| + |E|)$ $(\leq O(n^2))$ plus sorting
    $O(n\cdot{}log(n))$.
  \item Even with cycle detection, complexity remains the same
    (Tarjan's algorithm).
  \item Cycle breaking can be non-trivial, but $\ldots{}$
    \begin{itemize}
    \item The fewer (postinst) scripts, the fewer ``unbreakable cycles''.
    \item Remove all dependency cycles and this problem
        is indisputably ``trivial''.
    \item Yes, that is easier said than done, but that is at most what
      it takes.
    \end{itemize}
  \item Of course, if it is ``too simple'' for you, then you can
    always add more features on top to make it harder.
  \end{itemize}

  So, moving on $\ldots{}$
\end{frame}

\begin{frame}{The players in ``hard'' problem-game}
  Notable tools affected by the ``hard'' problem:
  \begin{itemize}
  \item APT, aptitude, cupt, etc.
  \item Britney
  \item DOSE, edos, etc.
  \end{itemize}

  Notable tools \textbf{not} affected by the ``hard'' problem:
  \begin{itemize}
  \item dpkg - it only verifies a given solution, which is polynomial.
  \item DAK's rm command - it does a ``cheap local''
    check.\footnote{It is primarily ``slow'' for other reasons}.
  \end{itemize}
\end{frame}

\section{The ``hard'' problem}


\begin{frame}{What makes the problem ``hard''?}
  \begin{itemize}
  \item The ``options''  (alternatives, virtual packages).
    \begin{itemize}
    \item This also includes ``normal'' $pkg (>= 1.0)$ versions.
    \item And especially unversioned dependencies with multiple
      versions of said packages.
    \end{itemize}
  \end{itemize}

  With options removed, everything else becomes piece of cake.
\end{frame}

\begin{frame}{What makes the problem ``hard''? Necessary evil}
  But what happens if we remove ``options''\footnote{You can also make
    it trivial in a different way.  However, it involves removing
    negative dependencies (which be ``fun'' in its own way)}:
  \begin{itemize}
  \item The problem is trivially reduced graph search $O(|V| \cdot |E|)$.
  \item Either we got no versioned dependencies OR we would have
    ``strictly equal'' versioned dependencies only.
  \item Without versioned dependencies, upgrades would be
    ``fun''.  Lots of ``fun''.
  \item With ``strictly equal'' versioned dependencies, \textit{every
    single} upload would be ``fun''.  Lots of ``fun''.
  \end{itemize}

  So the complexity is a necessary evil.
\end{frame}

\begin{frame}[fragile]
  \frametitle{An example of the problem}
  A simplified problem:
\begin{verbatim}
Package: coreutils
Version: 8.23-4
[...]
Depends: libc6 (>= 2.17)
\end{verbatim}
  Quiz: Given only that \textbf{libc6/2.19 is known to be
    installable}, can we immediately conclude that
  \textbf{coreutils/8.23-4 is also installable?}

  \begin{itemize}
  \item With negative dependencies?
  \item Without negatives dependencies?
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{An example of the problem - with answers}
  A simplified problem:
\begin{verbatim}
Package: coreutils
Version: 8.23-4
[...]
Depends: libc6 (>= 2.17)
\end{verbatim}
  Quiz: Given only that \textbf{libc6/2.19 is known to be
    installable}, can we immediately conclude that
  \textbf{coreutils/8.23-4 is also installable?}

  \begin{itemize}
  \item With negative dependencies: No, libc6 (et al) could have a
    Breaks/Conflicts on coreutils.
  \item Without negatives dependencies: Still no, libc6 (et al) could
    depend on \texttt{coreutils ($<<$ 8.23)} (or \texttt{coreutils ($>=$ 8.24)}).
  \end{itemize}

\end{frame}

\section{Highly tractable}


\begin{frame}{The big picture of the problem}
  In the big picture, we tend to optimize for (co-)installable
  packages.

  \begin{itemize}
  \item If there is Breaks/Conflicts, it will generally be versioned
    with an upper-bound (e.g. \texttt{Breaks: coreutils ($<<$ 8.23))}.
  \item If there is a (circular) dependency, it will generally
    unversioned or versioned with a lower-bound (e.g.  \texttt{Pre-Depends:
    coreutils ($>=$ 8.23)}).
  \item Alternatives/virtual packages are limited in numbers.
  \end{itemize}

  The major exceptions being: \ldots{}
\end{frame}

\begin{frame}{The big picture of the problem - the exceptions}
  The major exceptions being: \ldots{}
  \begin{itemize}
  \item Packages from ``unstable''.  Usually because packages are not
    built yet or ``occasionally'' due to transitions.
  \item ``mutually exclusive'' packages (e.g. providers of sendmail)
  \item Version ranges (``rare''): \texttt{foo (>= 1.0~), foo (<< 2.0~)} 
  \item Strictly equal versions: \texttt{foo (= \$\{binary:Version\})}
    \begin{itemize}
    \item almost exclusively used in binaries built from \textit{the
      same source}.
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Exponential run-time is all about choice!}

  \textbf{What makes the problem hard?}  In a nut shell, given:

\begin{verbatim}
Package: starting-package
Depends: foo1 | foo2 | foo3 | ... | fooN

Package: foo{1..N}
Depends: bar1 | bar2 | bar3 | ... | barM | good

Package: bar{1..M}
Depends: bad

Package: bad
Conflicts: starting-package

Package: good
\end{verbatim}

  Solve for \texttt{starting-package}.
  
\end{frame}

\begin{frame}{What makes the problem easy? We do!}

  \center{\textbf{\huge{Very few writes software like this.}}}

\end{frame}

\begin{frame}{What makes the problem easy? Except dictionaries}
  
  \center{\textbf{\huge{Very few writes software like this.}}}

  \begin{itemize}
  \item There are a handful of exceptions including aspell-dictionary,
    ispell-dictionary, wordlist, etc.
  \end{itemize}
\end{frame}

\begin{frame}{What makes the problem easy? Except dictionaries and Multi-Arch}
  
  \center{\textbf{\huge{Very few writes software like this.}}}

  \begin{itemize}
  \item There are a handful of exceptions including aspell-dictionary,
    ispell-dictionary, wordlist, etc.
  \item Technically, arch:any Multi-Arch foreign (or ``allowed'' with
    pkg:any dependencies) packages \textit{can} also cause
    ``unnecessary'' extra ``options'' as well.
  \end{itemize}
\end{frame}

\begin{frame}{If you think about it}
  If you think about it, to trigger this case (examplified):

  \begin{itemize}
  \item We would need $N$ distinct implementations of \texttt{awk}.
  \item They would have to depend on any one of $M$ distinct (but
    equally valid) \texttt{libc} implementations.
  \end{itemize}
\end{frame}

\begin{frame}{The dictionaries exampled}
  The reason the dictionaries ``blow up'' is because they work with
  pure interchangeable ``data''.

  \begin{itemize}
  \item Data packages themselves often have no or very trivial
    dependencies.
  \item The blow up often is limited to ``1 dependency level''.
  \end{itemize}

  Certainly, if you have $N$ of these ``1 dependency level'' blows up,
  you still have an issue.
\end{frame}

\section{Installing - part 1}

\begin{frame}{Installing in the easy case}
  Installing becomes trivial some common cases.  The conditions
  required:
  \begin{itemize}
  \item Single suite
  \item Single architecture
  \end{itemize}
\end{frame}

\begin{frame}{Concrete example: Lintian}
  Lintian is ``mostly trivial'':
  \begin{itemize}
  \item Lintian has 26 distinct dependency clauses
  \item With single suite and architecture, there is \textit{at most
    one package} satisfying each of its clauses.
  \item Some of its dependencies have ``non-trivial'' dependencies
    such as: \texttt{debconf (>= 1.2.0) | debconf-2.0}
  \end{itemize}

  By the time, the solver must ``guess'', all the dependencies of
  \texttt{debconf} have already been resolved.
\end{frame}

\begin{frame}{When do SS and SA happen?}
  Some particular common (mostly) polynomial cases:
  \begin{itemize}
  \item Installing build dependencies in ``clean'' unstable/stable Debian chroots (SAN)
  \item Installing packages in a pure Wheezy or Jessie system with Multi-Arch (SN)
  \item Installing packages in a pure Squeeze (or older) system (SAN)
  \item Installing packages in unstable+experimental OR stable +
    backports with ``strict'' preference for one of the suites (S)
  \item Testing migration (Britney) (SAN)
  \end{itemize}

  Legend:
  \begin{itemize}
  \item N: No special heuristics / code required.  The data itself
    exhibits the traits.
  \item S: ``single suite'' restriction (partially or fully)
  \item A: ``single architecture'' restriction (partially or fully)
  \end{itemize}
\end{frame}

\begin{frame}{Reasons for polynomial run-time}
  A couple of reasons why this happens:
  \begin{enumerate}
  \item With single suite + architecture, there is at most one version
    of any ``package/version/arch'' to satisfy dependencies.
  \item We forbid the use of alternatives implementations for
    libraries as it is fragile, which limits ``options'' in many
    packages.
  \item We only want so many implementations of awk, dpkg, etc.
  \item Breakage is usually quite ``obvious'' (despite a ``exploded
    options list'').
  \item We discourage (and in many cases) forbid the use of permanent
    negative dependencies.
  \end{enumerate}

  Can we somehow make it simple without the first item?
\end{frame}

\section{Upgrading in deterministic polynomial time}

\begin{frame}{Dist-upgrading - the basics}
  What happens when we do a simple stable to stable upgrade?
  \begin{itemize}
  \item Replace old release with new release in sources.list
  \item Have apt update its caches
  \item Have apt upgrade all packages with a new versioning
    in the new release
  \end{itemize}
\end{frame}


\begin{frame}{When is the dist-upgrade complete?}
  Long story short.  Upgrade is complete when all packages on the
  system:
  \begin{itemize}
  \item are also present in the new release,
    \begin{itemize}
    \item That is, old packages are removed.
    \end{itemize}
  \item have the same version as their counterpart in the release
    \begin{itemize}
    \item That is, they have been upgraded if there was a new version 
    \end{itemize}
  \item include the essential packages of the new release.
    \begin{itemize}
    \item This involves installing new packages, so we will ignore that.
    \end{itemize}    
  \end{itemize}
\end{frame}

\begin{frame}{Simplified upgrade scenario}
  Let us take a simple setup:
  \begin{itemize}
  \item Upgrade from Wheezy to Jessie (i.e. sed s/Wheezy/Jessie/g)
  \item Wheezy has \~{}30000 packages
  \item Jessie has \~{}35000 packages
  \item The system has 2000 packages installed from Wheezy
  \item Assume that all Wheezy packages got a new version in Jessie
  \end{itemize}
\end{frame}

\begin{frame}{Pop quiz on upgrades}
  \center{\textbf{\huge{What is the maximum problem size of this
        upgrade?}}}
  \begin{itemize}
  \item<1> 30000 packages
  \item<1> 35000 packages
  \item<1-> 37000 packages
  \item<1> 65000 packages
  \item<1> 67000 packages
  \item<1> Trick-question, the right answer is not on this slide
  \end{itemize}
\end{frame}

\begin{frame}{More on the upgrade}
  Further remarks:
  \begin{itemize}
  \item For every upgraded package, our problem size decreases by
    1 (\textit{without} Wheezy)
  \item Once we are done upgrading, we end up in a ``single suite''
    situation
    \begin{itemize}
    \item This is generally true for pure stable to stable upgrades
    \end{itemize}
  \item 65000 would be correct if we \textit{kept} Wheezy in the
    sources.list
  \end{itemize}
\end{frame}

\begin{frame}{Finding upgrades}
  Upgrading ought to be as simple as:
  \begin{itemize}
  \item Mark new essential packages for install
  \item Upgrade all binaries to their new version (if they have one)
  \item Remove all binaries not in the new suite
  \item Resolve missing dependencies if any
  \end{itemize}

  This basically solves upgrading by installing packages.  We can do
  better than this.
\end{frame}

\begin{frame}{On polynomial upgrades}
  A couple of remarks about deterministic polynomial upgrades:
  \begin{itemize}
  \item It is intended as a problem size reducer, not a perfect
    solution.
  \item It relies on the fact that solution verification is
    $O(|V| + |E|)$
  \item It exploits common ``patterns'' in dependencies.
  \end{itemize}
\end{frame}

\begin{frame}{Theory behind polynomial upgrades}
  \begin{itemize}
  \item We have a system that is \textbf{not} broken
    \begin{itemize}
    \item Theory: ``Given a set of installable mutually co-installable
      packages called $I$''
    \end{itemize}
  \item Where we can add, remove or replace one or more packages
    to/from/in this system (with another package) in linear time.
    \begin{itemize}
    \item Theory: ``We can compute
      $I_{new} = (I \setminus{} R) \cup{} A$ in linear time''
    \end{itemize}
  \item And then verify that the modified system is still \textbf{not}
    broken in polynomial time
    \begin{itemize}
    \item Theory: ``We verify that $I_{new}$ is a valid solution in
      $O(|I| + |E|)$''
    \end{itemize}
  \end{itemize}

  Issue of the day, how do we compute what to add/remove or replace (A
  and R)?
\end{frame}

% aptitude --with-recommends  install iceweasel icedove xfce4 evince eclipse lintian debhelper build-essential mplayer2 aspell-en xserver-xorg-input-all xscreensaver xserver-xorg xterm libreoffice python3-yaml emacs colord_ sane-utils_  dput
\begin{frame}{Finding packages in a jiffy}
  \begin{itemize}
  \item Group binaries from the same source together
    \begin{itemize}
    \item Due to ``Depends: \texttt{foo (= \$\{binary:Version\})}''
    \item Side-effect: Also resolves intra-source Breaks/Replaces.
    \end{itemize}
  \item Try upgrading each of these groups in (any) order
  \item If result is upgradable alone, commit
  \item Rinse and repeat until you reach a fix point (that is, nothing
    new can upgrade)
  \end{itemize}

\end{frame}

\begin{frame}{Results from simple upgrades}
  Depends on largely on what is installed:
  \begin{itemize}
  \item A number of packages could be upgraded
    \begin{itemize}
    \item Example test: libc6, ant, man-db*, eclipse*, xterm*, etc.
    \item Not tested in ``all configurations''
    \end{itemize}
  \item Basically, \texttt{foo (>= version-in-wheezy)}
    implies we can upgrade foo to the Jessie version.
    \begin{itemize}
    \item Common for libraries without ABI breaks
    \item Enables upgrading of ``stable'' packages before their
      reverse dependencies
    \end{itemize}
  \item Algorithm is fairly limited:
    \begin{itemize}
    \item Example issues: perl packages, python packages, etc.
    \end{itemize}
  \item Run-time: $O(|I|^2 * (|I| + |E|))$ (ballpark estimate)
  \end{itemize}
\end{frame}

\begin{frame}{Improving the upgrades}
  Further ideas:
  \begin{itemize}
  \item Combine groups if their relations might need it
    \begin{itemize}
    \item Example: dpkg needs new version of libc
    \item Example: dpkg breaks old version of readahead-fedora
    \end{itemize}
  \item Account for ``renamed'' packages (ABI breaks) and transitional
    packages
    \begin{itemize}
    \item Example: bar/wheezy depends on libfoo1, bar/jessie depends
      on libfoo2.
    \item Example: bar depends on foo.  In Jessie, foo depends on
      one single foo-replacement.
    \item In both cases, the replacement is built from the same
      source as the package being replaced.
    \end{itemize}
  \item If they have ``trivial'' (no-guess) solutions, resolve those.
    Otherwise, give up and try another package.
  \end{itemize}
\end{frame}

\section{Installing - part 2}

\begin{frame}{Smarter installing}
  We can also improve installing a bit:
  \begin{itemize}
  \item Find packages that are ``identical'' (apart from their
    ``name'' and ``version'')
  \item Find packages that are clearly ``superior'' to another one
  \end{itemize}

\end{frame}

\begin{frame}[fragile]
  \frametitle{Eqv. packages - staring into the abys}
  If you stare long enough at the ``dictionary problem'' (aspell-*
  etc.):
\begin{verbatim}
$ wdiff aspell-{ru,uk}
Package: [-aspell-ru-] {+aspell-uk+}
Version: [-0.99g5-20-] {+1.7.1-1+}
Architecture: all
Depends: aspell, dictionaries-common (>= 1.23~)
Provides: aspell-dictionary
\end{verbatim}
% aspell in oldstable, dict-common is in stable
  
\end{frame}

\begin{frame}{Eqv. packages - what is going on?}
  \begin{itemize}
  \item Textually the packages are different
  \item Semantically, they differ only by name and version
  \item Simplified view: Reverse dependencies \textit{could} in theory
    make them different
  \end{itemize}
\end{frame}

\begin{frame}{Find eqv. packages}
  \begin{itemize}
  \item They have the same (effective) dependencies
  \item They have the same (effective) negative dependencies
  \item They satisfy the same clauses of their reverse dependencies
  \end{itemize}
  This can be done in polynomial time for all packages.
\end{frame}

\begin{frame}{Substituable packages}
  Beyond eqv. packages, we can also find superior packages
  \begin{itemize}
  \item The pkgA have the same or fewer (effective) dependencies
    compared to pkgB
  \item The pkgA have the same or fewer (effective) negative
    dependencies compared to pkgB
  \item The pkgA satisfies at least all the same clauses of the
    reverse dependencies of pkgB
  \end{itemize}
\end{frame}

